FROM cloudron/base:3.2.0@sha256:ba1d566164a67c266782545ea9809dc611c4152e27686fd14060332dd88263ea

WORKDIR /app/code
RUN curl -sL https://github.com/nolanlawson/pinafore/archive/refs/tags/v2.2.0.tar.gz | tar --strip-components 1 -xvzf - && \ 
  yarn --production --pure-lockfile && \
  yarn build && \
  yarn cache clean && \
  rm -rf ./src ./docs ./tests ./bin

EXPOSE 4002

# Setting run-command, using explicit `node` command
# rather than `yarn` or `npm` to use less memory
# https://github.com/nolanlawson/pinafore/issues/971
CMD PORT=4002 node server.js
